#!/usr/bin/env bash

rm -Rf generated
mkdir -p generated
_createFile() {
	local filename="generated/$1"
	filenames+=($filename)
	declare -a a=("${!2}")
    declare -a b=("${!3}")
    local doTripleSlash=${4:-true}

	local c=()

	rm -f $filename
	echo "createFile $filename"
	echo -e "#status\turl\tmethod\tdata\texpected_result" >> $filename

	for((i=0;i<${#a[@]};i++));
	do
		if [[ ${#b[@]} -gt 0 ]]; then
			for ((j=0;j<${#b[@]};j++))
			do
				_createTrippleSlash ${a[i]}${b[j]} $doTripleSlash
			done
		else
			_createTrippleSlash ${a[i]} $doTripleSlash
		fi
	done

	for i in ${c[@]}
	do
		echo -e "404\t$i" >> $filename
	done

}

_createTrippleSlash() {
	local doTripleSlash=${2:-true}
	p=($1);
	c+=(${p});

	if ${doTripleSlash}; then
		pt=${p//\//\/\//}
		c+=(${pt});
	fi
}
_join() {
	local IFS="$1";
	shift;
	echo "$*";
}

filenames=()

CREDS=(admin:admin author:author)

semicolonSuffix=(
	\;%0aa.css \;%0aa.png \;%0aa.html \;%0aa.js
)

jsonSuffix=(
	.json .1.json .childrenlist.json .ext.json .4.2.1....json
	.json.html .json.css .json.js
	.json/a.html .json/a.css .json/a.js .json/a.png .json/a.jpg .json/a.jpeg .json/a.gif .json/a.ico
	.json\;%0aa.css .json\;%0aa.png .json\;%0aa.html .json\;%0aa.js
)
jsonRandomSuffix=(.json\;%0a{0}.css .json\;%0a{0}.png .html\;%0a{0}.css .html\;%0a{0}.png .json/{0}.css .json/{0}.js .json/{0}.png .json/a.gif .html/{0}.css .html/{0}.js .html/{0}.png .json/{0}.html)

extensionSuffix=(
	.html .css .js .ico .png .gif .jpg
)

query="?query=type:page%20limit:..1&pathPrefix="

#########################################
paths=(/apps /etc /home /libs /var)
_createFile "json.check" paths[@] jsonSuffix[@]

#########################################
paths=(/bin/querybuilder.json /bin/querybuilder.json.servlet /bin/querybuilder.feed /bin/querybuilder.feed.servlet)
	suffix=(
	.html .css .js .ico .png .gif .jpg
	.1.json .4.2.1...json
	/a.html /a.css /a.js /a.ico /a.png /a.gif /a.jpg
	/a.1.json /a.4.2.1...json
	\;%0aa.css \;%0aa.png \;%0aa.js \;%0aa.html \;%0aa.ico
)
_createFile "querybuilder.check" paths[@] suffix[@]

#########################################
paths=(/bin/wcm/search/gql.json)
suffix=(
	/a.1.json$query	/a.4.2.1...json$query /a.css$query /a.js$query /a.ico$query /a.png$query /a.html$query
	///a.1.json$query ///a.4.2.1...json$query ///a.css$query ///a.js$query ///a.ico$query ///a.png$query ///a.html$query
	\;%0aa.css$query \;%0aa.html$query \;%0aa.js$query \;%0aa.png$query \;%0aa.ico$query
)
_createFile "gql.check" paths[@] suffix[@]

#########################################
paths=(/libs/cq/cloudservicesprovisioning/content/autoprovisioning)
_createFile "autoprovisioning.check" paths[@] jsonSuffix[@]

#########################################
paths=(/system/sling/loginstatus)
_createFile "loginstatus.check" paths[@] jsonSuffix[@]

#########################################
paths=(/libs/granite/security/currentuser /libs/cq/security/userinfo)
_createFile "user.check" paths[@] jsonSuffix[@]

#########################################
paths=(/system/bgservlets/test)
_createFile "bgservlets.check" paths[@] jsonSuffix[@]

#########################################
paths=(/bin/wcm/search/gql.servlet.json)
suffix=($query)
_createFile "gql-servlet.check" paths[@] suffix[@]

#########################################
paths=(/system/console/bundles)
suffix=($query)
_createFile "bundles.check" paths[@] suffix[@]

#########################################
paths=(/crx/de/index.jsp /bin/crxde/logs{0}?tail=100 /crx/packmgr/index.jsp)
suffix=()
_createFile "crx.check" paths[@] suffix[@]

#########################################
paths=(/etc/reports/diskusage.html)
suffix=()
_createFile "reports.check" paths[@] suffix[@]

#########################################
paths=(/apps/system/config)
suffix=()
_createFile "config.check" paths[@] suffix[@]




#########################################
# POST
#########################################
paths=(/ /content /content/dam)
_createFile "post-content.check" paths[@] jsonSuffix[@]

#########################################
# POST
#########################################
paths=(/content/test /content/* /content/usergenerated/test /content/usergenerated/* /content/usergenerated/etc/commerce/smartlists/test /content/usergenerated/etc/commerce/smartlists/* /apps/test /apps/*)
_createFile "post-new-nodes.check" paths[@] jsonSuffix[@]


echo ---
echo all created files
echo ---
echo ${filenames[@]}

