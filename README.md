# README
The idea of this repository is to have a tool with which you can check your AEM instance for possible security risks easily and without further tools (except bash-console and curl).
It also provides a list of .check files with urls that can normally be used to compromise AEM.

# aem-dispatcher-check.sh
This is the main file. with this bash script you can run a dispatcher check.

//TODO

# create-urls.sh
//TODO

# .check file
The .check file contains the url to be checked.

## Data Structure
Each .check file should have the following structure:

| Column | Description |  
|---|---|
| status | HTTP Response Status Code |
| url | url which should be checked |
| method | GET or POST |
| data | data which should be added to the GET or POST call |
| expected_result | check if the response contains the expected result |

The column should be separated by \t (Tabulator)

## Example
```
#status	url	method	data	expected_result
404	/admin
404	/system/console
404	/dav/crx.default
404	/crx
404	/bin/crxde/logs
404	/jcr:system/jcr:versionStorage.json

404	/bin/groovyconsole/post.json	POST	script=printf %22takatuka%22&data=
```
