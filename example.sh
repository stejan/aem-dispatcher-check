#!/usr/bin/env bash
# vim: expandtab:ts=4:sw=4
###################################################
# Filename: example.sh
#   Status: draft
#  Purpose: AEM Dispatcher security check
#   Author: jan@stefam.ch
###################################################

protocol="https"
domain="www.example.com"
port=""

files="adobe.check example.check"

./aem-dispatcher-check.sh -d "$domain" -j "${protocol}" -f "${files}"

# only errors
./aem-dispatcher-check.sh -d "$domain" -j "${protocol}" -f "${files}" -e 2

# only errors
./aem-dispatcher-check.sh -d "$domain" -j "${protocol}" -f "${files}" -e 2 --proxy http://host:port
