#!/usr/bin/env bash

set -o errexit
set -o pipefail
#set -o nounset

url=""
protocol="http"
domain=""
port=":80"
insecure=""
user_agent="" #"User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
curl_params=""
verbosity=2
test_count=0
error_count=0
valid_page="/content"

declare -a files=("adobe.check")

_usage () {
	echo ""
	echo "Usage: $0 -d <domain> [OPTIONS]"
	echo "Options:"
	echo "   -c | --config-page <a config page path array without extension "
	echo "        - this can be a language or a website config page "
	echo "        - if you want to use two or more config pages then "
	echo "          you have to put the urls into quotes>"
	echo "        eg. -c /content/geometrixx"
	echo "            -c \"/content/geometrixx /content/geometrixx/de\" "
	echo "   -d | --domain <domain>"
	echo "   -D | --debug"
	echo "   -e | --error-level <loglevel> --> only errors are printed"
	echo "   -f | --files <space delimited list of files> "
	echo "   -h | --help"
	echo "   -i | --insecure - use insecure connection"
	echo "   -v | --valid-page <a valid page path without extension which is directly accessible - not just a content path>"
	echo "   -j | --protocol <http/https> protocol type"
	echo "   -p | --port <port> Port of the testing domain"
	echo "   -o | --output - No colors will be used"
	echo "   -q | --get-parameter <some parameter to add to the url to force cache avoidance>"
	echo "   -s | --get-selector <an enabled GET selector which could be used to extract more information e.g. \".contact-form.\">"
	echo "   -u | --user-agent - user agent string"
	echo "   -w | --curl-params <some additional curl parameters like \"--insecure\">"
	echo "   -z | --proxy Add a proxy to each curl call"
	echo ""
}

# Colors
colblk='\033[0;30m' # Black - Regular
colred='\033[0;31m' # Red
colora='\033[0;33m' # Orange
colgrn='\033[0;32m' # Green
colylw='\033[1;33m' # Yellow
colpur='\033[0;35m' # Purple
colrst='\033[0m'    # Text Reset

### verbosity levels
silent_lvl=0
crt_lvl=1
err_lvl=2
wrn_lvl=3
ntf_lvl=4
inf_lvl=5
dbg_lvl=6

function eok ()    { verb_lvl=$ntf_lvl elog "${colgrn}OK${colrst}  $@" ;}
function enok ()   { verb_lvl=$err_lvl elog "${colred}NOK${colrst} $@" ;}

## esilent prints output even in silent mode
function esilent () { verb_lvl=$silent_lvl elog "$@" ;}
function enotify () { verb_lvl=$ntf_lvl elog "$@" ;}
function ewarn ()   { verb_lvl=$wrn_lvl elog "${colylw}WARN${colrst} ---- $@" ;}
function einfo ()   { verb_lvl=$inf_lvl elog "${colwht}INFO${colrst} ---- $@" ;}
function edebug ()  { verb_lvl=$dbg_lvl elog "${colgrn}DEBUG${colrst} --- $@" ;}
function eerror ()  { verb_lvl=$err_lvl elog "${colred}ERROR${colrst} --- $@" ;}
function ecrit ()   { verb_lvl=$crt_lvl elog "${colpur}FATAL${colrst} --- $@" ;}
function edumpvar () { for var in $@ ; do edebug "$var=${!var}" ; done }
function elog() {
	if [ $verbosity -ge $verb_lvl ]; then
		echo -e "$@"
	fi
}

_abort () {
  if [ -z "$1" ]; then
    eerror "$0 Aborted" 3;
  else
    ecrit "Aborted. Reason: $1" 3;
  fi
  exit 1
}

_readParameters () {
	while [ "$1" != "" ]; do
		case "${1}" in
			-c | --config-page ) config_page=(${2// / }); shift;;
			-D | --debug ) set -x;;
			-d | --domain ) domain="${2}"; shift;;
			-e | --error-level ) verbosity="${2}"; shift;;
			-f | --files ) files=(${2// / }); shift;;
			-h | --help ) _usage; exit;;
			-i | --insecure ) insecure="--insecure";;
			-j | --protocol ) protocol="${2}"; shift;;
			-p | --port ) port=":${2}"; shift;;
			-o | --output ) colbkl='';colred='';colora='';colgrn='';colylw='';colput='';colrst='';;
			-q | --get-parameter ) get_parameter="${2}"; shift;;
			-s | --get-selector ) get_selector="${2}"; shift;;
			-u | --user-agent ) user_agent="-H $2"; shift;;
			-v | --valid-page ) valid_page="${2}"; shift;;
			-w | --curl-params ) curl_params="${2}"; shift;;
			-z | --proxy ) proxy="-x ${2}"; shift;;
			*) eerror "Unknown argument ${1}" 3; _usage; _abort;;
		esac
	  shift
	done

	# Check if port has default value
	if [[ "${port}" == ":80" ]]; then
		port=""
	fi

	url="${protocol}://${domain}${port}"
}

_contains () {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

_checkResponseCode (){
	rm -f headers.txt output.txt temp.txt

	test_count=$(($test_count+1))

	httpmethod='GET'

	if [ -n $3 ] && [ "$3" == 'POST' ]
	then
		httpmethod='POST'
	fi

	if [[ "$httpmethod" == 'POST' ]]
	then
		curl ${proxy} ${insecure} ${user_agent} ${curl_params} -s -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'Accept: */*' --data "$3" -D headers.txt -o output.txt "$url$2"
	else
		curl ${proxy} ${insecure} ${user_agent} ${curl_params} -s -D headers.txt -o output.txt "$url$2"
	fi

	httpStatus=$(head -1 headers.txt | awk '{print $2}')

	possibleHttpStates=(${1//,/ })

	if [[ $(_contains "${possibleHttpStates[@]}" "$httpStatus") == "y" ]]; then
		eok "[$httpStatus] $httpmethod $2"
	elif [[ "$httpStatus" == "301" ]] || [[ "$httpStatus" == "302" ]] || [[ "$httpStatus" == "303" ]] ; then
		redirectUrl=$(curl ${proxy} ${insecure} ${curl_params} -s -L -o temp.txt -w '%{url_effective}' "$url$2")
		if  [ "$httpStatus" == "$1" ]; then
			eok "[$httpStatus] $httpmethod $2 redirect to $redirectUrl"
		else
		    error_count=$((error_count+1))
			enok "[$httpStatus expected $1] $httpmethod $2 redirect to $redirectUrl"
		fi
		if [[ "${redirectUrl/https/http}" == "${url}$2" ]]; then
			ewarn "*WARN* Check if protocol is correct"
			ewarn "Called ${url}${2} -> $redirectUrl"
			exit 1
		fi
	elif [[ "$httpStatus" == "301" ]] && [[ "$httpStatus" == "$1" ]]; then
		redirectUrl=$(curl ${proxy} ${insecure} ${curl_params} -s -L -o temp.txt -w '%{url_effective}' "$url$2")
		if  [ "$httpStatus" == "$1" ]; then
			eok "[$httpStatus] $httpmethod $2 temporary redirect to $redirectUrl"
		else
		    error_count=$((error_count+1))
			enok "[$httpStatus expected $1] $httpmethod $2 temporary redirect to $redirectUrl"
		fi
		if [[ "${redirectUrl/https/http}" == "${url}$2" ]]; then
			ewarn "*WARN* Check if protocol is correct"
			ewarn "Called ${url}${2} -> $redirectUrl"
			exit 1
		fi
	else
		error_count=$((error_count+1))
		enok "[$httpStatus expected $1] $httpmethod $2"
	fi

}

_checkDispatcherFlush (){
	echo domain : ${domain}
	# Issue the following command in a terminal or command prompt to attempt to invalidate the Dispatcher cache, and ensure that you recieve a code 404 response:
	curl ${proxy} -s -D headers.txt -o output.txt -H "CQ-Handle: /libs" -H "CQ-Path: /libs" ${url}/dispatcher/invalidate.cache
	cat headers.txt
	httpStatus=$(head -1 headers.txt | awk '{print $2}')

	if [ "$httpStatus" != "404" ]; then
	    let "error_count=error_count+1"
		enok "[$httpStatus expected 404] for dispatcher flush"
	else
		eok "[$httpStatus] for dispatcher flush"
	fi
}

_getHttpStatus () {
	sed -i 's/HTTP\/1.1 200 Connection established//g' headers.txt
	sed -ri '/^\s*$/d' headers.txt

	echo $(head -1 headers.txt | awk '{print $2}')
}

_checkConfigPages (){
	curl ${proxy} -H "CQ-Handle: /libs" -H "CQ-Path: /libs" $url/dispatcher/invalidate.cache
}

_echoVariables (){
	edebug "url:        [$url]"
	edebug "protocol:   [$protocol]"
	edebug "domain:     [$domain]"
	edebug "port:       [$port]"
	edebug "insecure:   [$insecure]"
	edebug "proxy:      [$proxy]"
	edebug "valid-page: [$valid-page]"
	edebug "Files:"
	for file in "${files[@]}"
	do
		edebug "      $file"
	done
}

_main (){

	for file in "${files[@]}"
	do
		if [ -f "$file" ]
		then
			esilent ""
			esilent "$file found."

			while read -r line; do
                # ignore empty lines
                [[ -z $line ]] && continue
                [[ "$line" == $'\r' ]] && continue
                # ignore lines with beginning #
                [[ $line =~ ^#.* ]] && continue

                httpStatus=$(echo "$line" | cut -f 1)
                handle=$(echo "$line" | cut -f 2)
                method=$(echo "$line" | cut -f 3)
                data=$(echo "$line" | cut -f 4)
                result=$(echo "$line" | cut -f 5)

                if [ "${httpStatus}" == "${handle}" ]; then
                	eerror "ERROR while reading file : ${file}"
                	eerror "Are tabs correct in line : ${line}"
                	exit -1
                fi

                _checkResponseCode "${httpStatus}" "${handle/addValidPathToPage/$valid_page}" "${method}" "${data}" "$result"
			done < "$file"

			esilent "finished."
		else
			esilent ""
			ewarn "$file not found."
		fi
	done

	esilent ""
	esilent "num of tests / errors : $test_count / $error_count"

}

######################################################
# MAIN

# check parameters
if [[ $# -eq 0 ]]; then
	echo "No arguments supplied"
	_usage
	exit
fi

# read console parameters
_readParameters "$@"
esilent "Dispatcher check for url: ${url}"
_echoVariables
_main

rm -f headers.txt output.txt temp.txt
######################################################
if [[ $error_count -eq 0 ]]; then
	exit 0
else 
	exit 1
fi